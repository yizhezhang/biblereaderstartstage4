package bibleReader;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import bibleReader.model.Bible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.BookOfBible;
import bibleReader.model.NavigableResults;
import bibleReader.model.Reference;
import bibleReader.model.ReferenceList;
import bibleReader.model.ResultType;

/**
 * The display panel for the Bible Reader.
 * 
 * @author cusack
 * @author Jonathan Chaffer & Jason Gombas (2018)
 * @author Jason Gombas & Yizhe Zhang (Stage 7, 9 and 10)
 */
public class ResultView extends JPanel {
	private BibleReaderModel bibleModel;
	private JScrollPane scrollPane;
	public JEditorPane editorPane;
	private JTextArea stats;
	public JButton previousButton;
	public JButton nextButton;
	private NavigableResults navResults;
	private JTextArea pageStatus;

	/**
	 * Construct a new ResultView and set its model to myModel.
	 * 
	 * @param myModel
	 *            The model this view will access to get information.
	 */
	public ResultView(BibleReaderModel myModel) {
		bibleModel = myModel;
		navResults = new NavigableResults(new ReferenceList(), "", ResultType.NONE);
		setupGUI();
	}

	/**
	 * Sets up the main components of ResultView. Should be called once in the
	 * constructor.
	 */
	private void setupGUI() {
		previousButton = new JButton("Previous");
		previousButton.setName("PreviousButton");
		previousButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updatePrevious();
			}
		});
		nextButton = new JButton("Next");
		nextButton.setName("NextButton");
		nextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateNext();
			}
		});
		pageStatus = new JTextArea("");
		editorPane = new JEditorPane();
		editorPane.setName("OutputEditorPane");
		scrollPane = new JScrollPane(editorPane);
		editorPane.setContentType("text/html");
		stats = new JTextArea("");
		stats.setEditable(false);
		editorPane.setEditable(false);
		setLayout(new BorderLayout());
		add(scrollPane, BorderLayout.CENTER);
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new FlowLayout());
		bottomPanel.add(stats);
		bottomPanel.add(pageStatus);
		bottomPanel.add(previousButton);
		bottomPanel.add(nextButton);
		add(bottomPanel, BorderLayout.SOUTH);
	}

	/**
	 * Updates ResultView when a search has been made.
	 * 
	 * @param input
	 */
	public void updateSearch(String input) {
		ReferenceList allReferences = bibleModel.getReferencesContaining(input);
		navResults = new NavigableResults(allReferences, input, ResultType.SEARCH);
		String text = getHTML(navResults.currentResults());
		String finalText = text.replaceAll("(?i)" + input, "<b>$0</b>");
		editorPane.setText(finalText);
		updateStatsSearch(navResults.size(), input);
		enableDisableButtons();
		updatePageStatus();
		editorPane.setCaretPosition(0);
	}

	/**
	 * Updates the ResultView when searching a passage.
	 * 
	 * @param input
	 */
	public void updatePassage(String input) {
		ReferenceList allReferences = bibleModel.getReferencesForPassage(input);
		navResults = new NavigableResults(allReferences, input, ResultType.PASSAGE);
		ReferenceList list = navResults.currentResults();
		if (list.size() == 0) {
			editorPane.setText("No Results");
		} else {
			String header = getHeaderHTML(list.get(0), list.get(list.size() - 1));
			editorPane.setText(header + getHTML(list));
		}
		updateStatsPassage(navResults.size(), input);
		enableDisableButtons();
		updatePageStatus();
		editorPane.setCaretPosition(0);
	}

	/**
	 * Updates the Stats JTextArea when search is pressed.
	 * 
	 * @param num
	 * @param input
	 */
	private void updateStatsSearch(int num, String input) {
		stats.setText("There are " + num + " verses containing " + input);
	}

	/**
	 * Updates the Stats JTextArea when passage is pressed.
	 * 
	 * @param input
	 */
	private void updateStatsPassage(int size, String input) {
		if (size == 0) {
			stats.setText("Your input \"" + input + "\" was invalid");
		} else {
			stats.setText("You requested " + input + ". There were " + size + " results.");
		}
	}

	/**
	 * Returns the HTML text from a search.
	 * 
	 * @param input
	 * @return A String which contains basic HTML code that formats search results
	 *         in a table format.
	 */
	private String getHTML(ReferenceList refList) {
		StringBuffer html = new StringBuffer("");
		int num = 0;
		html.append("<table><tbody>");

		// The following gives HTML for SEARCH ResultType
		if (navResults.getType() == ResultType.SEARCH) {
			html.append("<tr><td valign=\"top\" width=\"100\">Verse</td>");
			for (String version : bibleModel.getVersions()) {
				html.append("<td>");
				html.append(version);
				html.append("</td>");
			}
			for (Reference ref : refList) {
				num++;
				html.append("<tr><td valign='top'>").append(ref.toString()).append("</td>");
				for (String version : bibleModel.getVersions()) {
					Bible bible = bibleModel.getBible(version);
					html.append("<td>");
					String text = bible.getVerseText(ref);
					if (text != null) {
						html.append(text);
					}
					html.append("</td>");
				}
				html.append("</tr>");
			}
		}
		// The following gives HTML for PASSAGE ResultType
		else if (navResults.getType() == ResultType.PASSAGE) {
			html.append("<tr>");
			for (String version : bibleModel.getVersions()) {
				html.append("<td>");
				html.append(version);
				html.append("</td>");
			}
			html.append("</tr><tr>");
			for (String version : bibleModel.getVersions()) {
				html.append("<td  valign='top'>");
				Bible bible = bibleModel.getBible(version);
				int currentChapter = refList.get(0).getChapter();
				for (Reference ref : refList) {
					num++;
					String text = bible.getVerseText(ref);
					if (text != null) {
						if (currentChapter == ref.getChapter()) {
							html.append("<sup>").append(ref.getVerse()).append("</sup>");
							html.append(text);
						} else {
							html.append("<p></p><br>");
							html.append("<sup>").append(ref.getChapter()).append("</sup>");
							html.append(text);
							currentChapter = ref.getChapter();
						}
					}
				}
				html.append("</td>");
			}
			html.append("</tr>");
		}
		if (num == 0) {
			return "No Results";
		}
		return html.toString();
	}

	/**
	 * This is the method used in the ActionPerformed method for the next button. It
	 * properly updates the screen.
	 */
	private void updateNext() {
		if (navResults.hasNextResults()) {
			if (navResults.getType() == ResultType.PASSAGE) {
				ReferenceList list = navResults.nextResults();
				String header = getHeaderHTML(list.get(0), list.get(list.size() - 1));
				editorPane.setText(header + getHTML(list));
			} else if (navResults.getType() == ResultType.SEARCH) {
				editorPane.setText(getHTML(navResults.nextResults()).replaceAll("(?i)" + navResults.getQueryPhrase(),
						"<b>$0</b>"));
			}
		}
		enableDisableButtons();
		updatePageStatus();
		editorPane.setCaretPosition(0);
	}

	/**
	 * This is the method used in the ActionPerformed method for the previous
	 * button. It properly updates the screen.
	 */
	private void updatePrevious() {
		if (navResults.hasPreviousResults()) {
			if (navResults.getType() == ResultType.PASSAGE) {
				ReferenceList list = navResults.previousResults();
				String header = getHeaderHTML(list.get(0), list.get(list.size() - 1));
				editorPane.setText(header + getHTML(list));
			} else if (navResults.getType() == ResultType.SEARCH) {
				editorPane.setText(getHTML(navResults.previousResults())
						.replaceAll("(?i)" + navResults.getQueryPhrase(), "<b>$0</b>"));
			}
		}
		enableDisableButtons();
		updatePageStatus();
		editorPane.setCaretPosition(0);
	}

	/**
	 * A method that enables and disables buttons according to the current status
	 */
	private void enableDisableButtons() {
		if (!navResults.hasNextResults()) {
			nextButton.setEnabled(false);
		} else {
			nextButton.setEnabled(true);
		}
		if (!navResults.hasPreviousResults()) {
			previousButton.setEnabled(false);
		} else {
			previousButton.setEnabled(true);
		}
	}

	/**
	 * Updates the pageStatus text field to show the correct page numbers.
	 */
	private void updatePageStatus() {
		pageStatus.setText("Showing page " + navResults.getPageNumber() + " of " + navResults.getNumberPages());
	}

	/**
	 * This is a helper method to get an appropriate description of a search given
	 * the two references.
	 * 
	 * @param ref1
	 * @param ref2
	 * @return
	 */
	private String getHeaderHTML(Reference ref1, Reference ref2) {
		StringBuffer sb = new StringBuffer();
		sb.append("<center><b>");
		BookOfBible book1 = ref1.getBookOfBible();
		BookOfBible book2 = ref2.getBookOfBible();
		int chapter1 = ref1.getChapter();
		int chapter2 = ref2.getChapter();
		int verse2 = ref2.getVerse();
		if (book1 == book2) {
			if (chapter1 == chapter2) {
				sb.append(ref1.toString()).append("-").append(verse2);
			} else {
				sb.append(ref1.toString()).append("-").append(chapter2).append(":").append(verse2);
			}

		} else {
			sb.append(ref1).append("-").append(ref2);
		}
		sb.append("</b></center>");
		return sb.toString();
	}
}
