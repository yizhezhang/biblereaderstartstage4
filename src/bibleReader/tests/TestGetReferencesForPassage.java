package bibleReader.tests;

import static org.junit.Assert.assertArrayEquals;
import java.io.File;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import bibleReader.BibleIO;
import bibleReader.model.Bible;
import bibleReader.model.BibleFactory;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.ReferenceList;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * Tests for the getReferencesForPassage method in the BibleReaderModel class.
 * These tests assume BibleIO is working an can read in the kjv.atv file.
 * 
 * @author Chuck Cusack, January, 2013
 * @author Jason Gombas & Yizhe Zhang, February, 2018
 */
public class TestGetReferencesForPassage {
	private static VerseList versesFromFile;
	private BibleReaderModel model;

	@BeforeClass
	public static void readFile() {
		// Our tests will be based on the KJV version for now.
		File file = new File("kjv.atv");
		// We read the file here so it isn't done before every test.
		versesFromFile = BibleIO.readBible(file);
	}

	@Before
	public void setUp() throws Exception {
		// Make a shallow copy of the verses.
		ArrayList<Verse> copyOfList = new ArrayList<Verse>(versesFromFile);
		// Now make a copy of the VerseList
		VerseList copyOfVerseList = new VerseList(versesFromFile.getVersion(), versesFromFile.getDescription(),
				copyOfList);

		Bible testBible = BibleFactory.createBible(copyOfVerseList);
		model = new BibleReaderModel();
		model.addBible(testBible);
	}

	public VerseList getVersesForReference(String reference) {
		ReferenceList list = model.getReferencesForPassage(reference);
		VerseList results = model.getVerses("KJV", list);
		return results;
	}

	@Test(timeout = 5000)
	public void testSpecificSingleVerseCases() {
		VerseList testList1 = getVersesForReference("John 3 : 16");
		VerseList trueResult1 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult1.add(versesFromFile.get(26136));
		assertArrayEquals(trueResult1.toArray(), testList1.toArray());

		VerseList testList2 = getVersesForReference("Gen 1:1");
		VerseList trueResult2 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult2.add(versesFromFile.get(0));
		assertArrayEquals(trueResult2.toArray(), testList2.toArray());

		VerseList testList3 = getVersesForReference("Revelation 22:21");
		VerseList trueResult3 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult3.add(versesFromFile.get(31101));
		assertArrayEquals(trueResult3.toArray(), testList3.toArray());
	}

	@Test(timeout = 5000)
	public void testSpecificMultiVerse() {
		VerseList testList1 = getVersesForReference(" Ecclesiastes 3 : 1 - 8 ");
		VerseList trueResult1 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult1.addAll(versesFromFile.subList(17360, 17368));
		assertArrayEquals(trueResult1.toArray(), testList1.toArray());

		VerseList testList2 = getVersesForReference("Joshua 24:28-33");
		VerseList trueResult2 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult2.addAll(versesFromFile.subList(6504, 6510));
		assertArrayEquals(trueResult2.toArray(), testList2.toArray());

		VerseList testList3 = getVersesForReference("Psalm 23:1-6");
		VerseList trueResult3 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult3.addAll(versesFromFile.subList(14236, 14242));
		assertArrayEquals(trueResult3.toArray(), testList3.toArray());
	}

	@Test(timeout = 5000)
	public void testSpecificChapter() {
		VerseList testList1 = getVersesForReference("Song of Solomon 3");
		VerseList trueResult1 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult1.addAll(versesFromFile.subList(17572, 17583));
		assertArrayEquals(trueResult1.toArray(), testList1.toArray());

		VerseList testList2 = getVersesForReference("Revelation 22");
		VerseList trueResult2 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult2.addAll(versesFromFile.subList(31081, 31102));
		assertArrayEquals(trueResult2.toArray(), testList2.toArray());

		VerseList testList3 = getVersesForReference("1 Tim 2-4");
		VerseList trueResult3 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult3.addAll(versesFromFile.subList(29717, 29764));
		assertArrayEquals(trueResult3.toArray(), testList3.toArray());

		VerseList testList4 = getVersesForReference("1 John 2-3");
		VerseList trueResult4 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult4.addAll(versesFromFile.subList(30551, 30604));
		assertArrayEquals(trueResult4.toArray(), testList4.toArray());
	}

	@Test(timeout = 5000)
	public void testSpecificVersesFromMultipleChapters() {
		VerseList testList1 = getVersesForReference("Isa 52:13 - 53:12");
		VerseList trueResult1 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult1.addAll(versesFromFile.subList(18709, 18724));
		assertArrayEquals(trueResult1.toArray(), testList1.toArray());

		VerseList testList2 = getVersesForReference("Mal 3:6-4:6");
		VerseList trueResult2 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult2.addAll(versesFromFile.subList(23126, 23145));
		assertArrayEquals(trueResult2.toArray(), testList2.toArray());
	}

	@Test(timeout = 5000)
	public void testWholeBook() {
		VerseList testList1 = getVersesForReference("1 Kings");
		VerseList trueResult1 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult1.addAll(versesFromFile.subList(8718, 9534));
		assertArrayEquals(trueResult1.toArray(), testList1.toArray());

		VerseList testList2 = getVersesForReference("Philemon");
		VerseList trueResult2 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult2.addAll(versesFromFile.subList(29939, 29964));
		assertArrayEquals(trueResult2.toArray(), testList2.toArray());
	}

	@Test(timeout = 5000)
	public void testOddestSyntax() {
		VerseList testList1 = getVersesForReference("Ephesians 5-6:9");
		VerseList trueResult1 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult1.addAll(versesFromFile.subList(29305, 29347));
		assertArrayEquals(trueResult1.toArray(), testList1.toArray());

		VerseList testList2 = getVersesForReference("Hebrews 11-12:2");
		VerseList trueResult2 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		trueResult2.addAll(versesFromFile.subList(30173, 30215));
		assertArrayEquals(trueResult2.toArray(), testList2.toArray());
	}

	@Test(timeout = 5000)
	public void testInvalidBookChapterVerse() {
		VerseList testList1 = getVersesForReference("Jude 2");
		VerseList trueResult1 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		assertArrayEquals(trueResult1.toArray(), testList1.toArray());

		VerseList testList2 = getVersesForReference("Herman 2");
		VerseList trueResult2 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		assertArrayEquals(trueResult2.toArray(), testList2.toArray());

		VerseList testList3 = getVersesForReference("John 3:163");
		VerseList trueResult3 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		assertArrayEquals(trueResult3.toArray(), testList3.toArray());

		VerseList testList4 = getVersesForReference("Mal 13:6-24:7");
		VerseList trueResult4 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		assertArrayEquals(trueResult4.toArray(), testList4.toArray());
	}

	@Test(timeout = 5000)
	public void testInvalidSyntaxAndChapterVerseOutOfOrder() {
		VerseList testList1 = getVersesForReference("1 Tim 3-2");
		VerseList trueResult1 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		assertArrayEquals(trueResult1.toArray(), testList1.toArray());

		VerseList testList2 = getVersesForReference("Deut :2-3");
		VerseList trueResult2 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		assertArrayEquals(trueResult2.toArray(), testList2.toArray());

		VerseList testList3 = getVersesForReference("Josh 6:4- :6");
		VerseList trueResult3 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		assertArrayEquals(trueResult3.toArray(), testList3.toArray());

		VerseList testList4 = getVersesForReference("Ruth : - :");
		VerseList trueResult4 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		assertArrayEquals(trueResult4.toArray(), testList4.toArray());

		VerseList testList5 = getVersesForReference("2 Sam : 4-7");
		VerseList trueResult5 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		assertArrayEquals(trueResult5.toArray(), testList5.toArray());

		VerseList testList6 = getVersesForReference("Ephesians 5:2,4");
		VerseList trueResult6 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		assertArrayEquals(trueResult6.toArray(), testList6.toArray());

		VerseList testList7 = getVersesForReference("John 3;16");
		VerseList trueResult7 = new VerseList("KJV", "Holy Bible, Authorized (King James) Version");
		assertArrayEquals(trueResult7.toArray(), testList7.toArray());
	}
}
