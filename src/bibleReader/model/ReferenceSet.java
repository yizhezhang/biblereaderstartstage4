package bibleReader.model;

import java.util.Collection;
import java.util.TreeSet;

public class ReferenceSet extends TreeSet<Reference> {
	
	public ReferenceSet() {
		
	}

	public ReferenceSet(Collection<? extends Reference> set) {
		super(set);
	}
	
}
