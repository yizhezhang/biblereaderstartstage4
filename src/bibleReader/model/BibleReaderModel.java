package bibleReader.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The model of the Bible Reader. It stores the Bibles and has methods for
 * searching for verses based on words or references.
 * 
 * @author cusack
 * @author Jon Chaffer & Jason Gombas
 * @author Jason Gombas & Yizhe Zhang (Stage 7 and 9)
 */
public class BibleReaderModel implements MultiBibleModel {
	private ArrayList<Bible> bibles;
	private ArrayList<Concordance> concordances;

	/**
	 * Default constructor. You probably need to instantiate objects and do other
	 * assorted things to set up the model.
	 */
	public BibleReaderModel() {
		bibles = new ArrayList<Bible>();
		concordances = new ArrayList<Concordance>();
	}

	@Override
	public String[] getVersions() {
		TreeSet<String> versionSet = new TreeSet<String>();
		for (int i = 0; i < bibles.size(); i++) {
			versionSet.add(bibles.get(i).getVersion());
			;
		}
		String[] versions = new String[versionSet.size()];
		int i = 0;
		for (String version : versionSet) {
			versions[i] = version;
			i++;
		}
		return versions;
	}

	@Override
	public int getNumberOfVersions() {
		return getVersions().length;
	}

	@Override
	public void addBible(Bible bible) {
		bibles.add(bible);
		concordances.add(new Concordance(bible));
	}

	@Override
	public Bible getBible(String version) {
		for (Bible bible : bibles) {
			if (bible.getVersion().equals(version)) {
				return bible;
			}
		}
		return null;
	}

	@Override
	public ReferenceList getReferencesContaining(String words) {
		ReferenceSet refsToReturn = new ReferenceSet();
		for (Bible bible : bibles) {
			for (Reference ref : bible.getReferencesContaining(words)) {
				refsToReturn.add(ref);
			}
		}
		return new ReferenceList(refsToReturn);
	}

	@Override
	public VerseList getVerses(String version, ReferenceList references) {
		for (Bible bible : bibles) {
			if (bible.getVersion().equals(version)) {
				return bible.getVerses(references);
			}
		}
		return null;
	}

	@Override
	public String getText(String version, Reference reference) {
		if (getBible(version) == null) {
			return "";
		}
		Verse result = getBible(version).getVerse(reference);
		if (result == null) {
			return "";
		}
		return result.getText();
	}

	@Override
	public ReferenceList getReferencesForPassage(String reference) {
		ReferenceList refList = new ReferenceList();
		reference = reference.replaceAll("\\s+", "").toLowerCase();
		String book = "";
		BookOfBible bookOfBible;
		String numbers;
		Pattern pattern = Pattern.compile("([1-3]?[a-z]+)");
		Matcher matcher = pattern.matcher(reference);
		if (matcher.find()) {
			book = matcher.group(1);
		}
		numbers = reference.replaceAll("([1-3]?[a-z]+)", "");
		if (BookOfBible.getBookOfBible(book) == null) {
			return refList;
		} else {
			bookOfBible = BookOfBible.getBookOfBible(book);
		}
		try {
			if (numbers.equals("")) {
				// return list with entire book
				return getBookReferences(bookOfBible);
			}
			String[] split = numbers.split("[:-]");
			if (split.length == 1) {
				// return list with one chapter
				int chapter = Integer.parseInt(numbers);
				return getChapterReferences(bookOfBible, chapter);
			} else if (split.length == 2) {
				if (numbers.contains("-")) {
					// return list between two chapters
					int chapter1 = Integer.parseInt(split[0]);
					int chapter2 = Integer.parseInt(split[1]);
					return getChapterReferences(bookOfBible, chapter1, chapter2);
				} else {
					// return list with one verse
					int chapter = Integer.parseInt(split[0]);
					int verse = Integer.parseInt(split[1]);
					return getVerseReferences(bookOfBible, chapter, verse);
				}
			} else if (split.length == 3) {
				String[] testSplit = numbers.split("-");
				if (testSplit[0].contains(":") && testSplit.length == 2) {
					// return list in chapter, between two verses
					int chapter1 = Integer.parseInt(split[0]);
					int verse1 = Integer.parseInt(split[1]);
					int verse2 = Integer.parseInt(split[2]);
					return getPassageReferences(bookOfBible, chapter1, verse1, verse2);
				} else if (testSplit.length == 2) {
					// return list between two chapters, from first chapter verse 1 to second
					// verse with the specified verse
					int chapter1 = Integer.parseInt(split[0]);
					int chapter2 = Integer.parseInt(split[1]);
					int verse2 = Integer.parseInt(split[2]);
					int verse1 = 1;
					return getPassageReferences(bookOfBible, chapter1, verse1, chapter2, verse2);
				}
			} else if (split.length == 4) {
				// return list between two chapters with two verses
				int chapter1 = Integer.parseInt(split[0]);
				int verse1 = Integer.parseInt(split[1]);
				int chapter2 = Integer.parseInt(split[2]);
				int verse2 = Integer.parseInt(split[3]);
				return getPassageReferences(bookOfBible, chapter1, verse1, chapter2, verse2);
			}
		} catch (NumberFormatException e) {
			// We expect NumberFormatExceptions
			return refList;
		}
		return refList;
	}

	@Override
	public ReferenceList getVerseReferences(BookOfBible book, int chapter, int verse) {
		ReferenceSet refSet = new ReferenceSet();
		if (bibles.size() > 0) {
			for (Bible bible : bibles) {
				Verse testVerse = bible.getVerse(book, chapter, verse);
				if (testVerse != null) {
					refSet.add(bible.getVerse(book, chapter, verse).getReference());
				}
			}
		}
		return new ReferenceList(refSet);
	}

	@Override
	public ReferenceList getPassageReferences(Reference startVerse, Reference endVerse) {
		ReferenceSet refSet = new ReferenceSet();
		if (bibles.size() > 0) {
			for (Bible bible : bibles) {
				refSet.addAll(bible.getReferencesInclusive(startVerse, endVerse));

			}
		}
		return new ReferenceList(refSet);
	}

	@Override
	public ReferenceList getBookReferences(BookOfBible book) {
		ReferenceSet refSet = new ReferenceSet();
		if (bibles.size() > 0) {
			for (Bible bible : bibles) {
				refSet.addAll(bible.getReferencesForBook(book));
			}
		}
		return new ReferenceList(refSet);
	}

	@Override
	public ReferenceList getChapterReferences(BookOfBible book, int chapter) {
		ReferenceSet refSet = new ReferenceSet();
		if (bibles.size() > 0) {
			for (Bible bible : bibles) {
				refSet.addAll(bible.getReferencesForChapter(book, chapter));
			}
		}
		return new ReferenceList(refSet);
	}

	@Override
	public ReferenceList getChapterReferences(BookOfBible book, int chapter1, int chapter2) {
		ReferenceSet refSet = new ReferenceSet();
		if (bibles.size() > 0) {
			for (Bible bible : bibles) {
				refSet.addAll(bible.getReferencesForChapters(book, chapter1, chapter2));
			}
		}
		return new ReferenceList(refSet);
	}

	@Override
	public ReferenceList getPassageReferences(BookOfBible book, int chapter, int verse1, int verse2) {
		ReferenceSet refSet = new ReferenceSet();
		if (bibles.size() > 0) {
			for (Bible bible : bibles) {
				refSet.addAll(bible.getReferencesForPassage(book, chapter, verse1, verse2));
			}
		}
		return new ReferenceList(refSet);
	}

	@Override
	public ReferenceList getPassageReferences(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		ReferenceSet refSet = new ReferenceSet();
		if (bibles.size() > 0) {
			for (Bible bible : bibles) {
				refSet.addAll(bible.getReferencesForPassage(book, chapter1, verse1, chapter2, verse2));
			}
		}
		return new ReferenceList(refSet);
	}

	// ------------------------------------------------------------------
	// These are the better searching methods.
	//
	@Override
	public ReferenceList getReferencesContainingWord(String word) {
		TreeSet<Reference> result = new TreeSet<Reference>();
		for (Concordance concordance : concordances) {
			result.addAll(concordance.getReferencesContaining(word));
		}
		return new ReferenceList(result);
	}

	@Override
	public ReferenceList getReferencesContainingAllWords(String words) {
		ArrayList<String> multiWords = Concordance.extractWords(words);
		TreeSet<Reference> result = new TreeSet<Reference>();
		for (Concordance concordance : concordances) {
			result.addAll(concordance.getReferencesContainingAll(multiWords));
		}
		return new ReferenceList(result);
	}

	private ArrayList<ArrayList<String>> getSearchRequirements(String search) {
		ArrayList<String> phraseList = new ArrayList<String>();
		ArrayList<String> wordList = new ArrayList<String>();
		ArrayList<ArrayList<String>> results = new ArrayList<ArrayList<String>>();
		results.add(phraseList);
		results.add(wordList);
		String[] split = search.split("\"", 3);
		if (split.length < 3) {
			if (!search.equals("")) {
				// Add the entire search to the word search
				results.get(1).addAll(Concordance.extractWords(search));
			}
		} else {
			String first = split[0];
			String second = split[1];
			String third = split[2];
			if (!first.equals("")) {
				// Add first entries to word list
				results.get(1).addAll(Concordance.extractWords(first));
			}
			if (!second.equals("")) {
				// Add second to phrase list
				results.get(0).add(second.toLowerCase());
			}
			ArrayList<ArrayList<String>> recursionResults = getSearchRequirements(third);
			results.get(0).addAll(recursionResults.get(0));
			results.get(1).addAll(recursionResults.get(1));
		}

		return results;
	}

	@Override
	public ReferenceList getReferencesContainingAllWordsAndPhrases(String words) {
		ArrayList<ArrayList<String>> searchRequirements = getSearchRequirements(words);
		ArrayList<String> phraseList = searchRequirements.get(0);
		ArrayList<String> wordList = searchRequirements.get(1);
		StringBuffer sb = new StringBuffer();
		for (String word : wordList) {
			sb.append(word);
			sb.append(" ");
		}
		if (wordList.size() == 0) {
			return phraseSearch(phraseList);
		} else if (phraseList.size() == 0) {
			TreeSet<Reference> wordResult = new TreeSet<Reference>();
			wordResult.addAll(getReferencesContainingAllWords(sb.toString()));
			return new ReferenceList(wordResult);
		} else {
			ReferenceList results = phraseSearch(wordList, phraseList);
			// for (Reference ref : results) {
			// System.out.println(ref);
			//
			// }
			return results;
		}

		// ReferenceList results = new ReferenceList();
		// // for (Reference ref : phraseResults) {
		// // for (String word : wordList) {
		// // ReferenceList referencesWithWord = getReferencesContainingWord(word);
		// // if (referencesWithWord.contains(ref)) {
		// // results.add(ref);
		// // }
		// // }
		// // }
		// if (wordList.size() != 0) {
		// results = getReferencesContainingAllWords(sb.toString());
		// results.retainAll(phraseResults);
		//// for (Reference ref : results) {
		//// System.out.println(ref);
		//// }
		// return results;
		// } else {
		// return phraseResults;
		// }

	}

	private ReferenceList phraseSearch(ArrayList<String> phrases) {
		ReferenceList possibilities = new ReferenceList();
		StringBuffer sb = new StringBuffer();
		for (String phrase : phrases) {
			sb.append(phrase);
			sb.append(" ");
		}
		possibilities.addAll(getReferencesContainingAllWords(sb.toString()));
		TreeSet<Reference> results = new TreeSet<Reference>();
		for (Bible bible : bibles) {
			for (Reference ref : possibilities) {
				boolean containsAllPhrases = true;
				for (String phrase : phrases) {
					String text = bible.getVerseText(ref);
					if (text != null && !text.equals("")) {
						String x = "\\b" + phrase + "\\b";
						Pattern pattern = Pattern.compile(x);
						Matcher matcher = pattern.matcher(text.toLowerCase());
						if (!matcher.find()) {
							containsAllPhrases = false;
						}
					} else {
						containsAllPhrases = false;
					}
				}
				if (containsAllPhrases) {
					results.add(ref);
				}
			}
		}
		return new ReferenceList(results);
	}

	private ReferenceList phraseSearch(ArrayList<String> words, ArrayList<String> phrases) {
		ReferenceList possibilities = new ReferenceList();
		StringBuffer sb = new StringBuffer();
		for (String phrase : phrases) {
			sb.append(phrase);
			sb.append(" ");
		}
		possibilities.addAll(getReferencesContainingAllWords(sb.toString()));
		TreeSet<Reference> results = new TreeSet<Reference>();
		for (Bible bible : bibles) {
			for (Reference ref : possibilities) {
				boolean containsAllPhrases = true;
				String text = bible.getVerseText(ref);
				if (text != null) {
					String lowTxt = text.toLowerCase();
					for (String phrase : phrases) {
						String x = "\\b" + phrase + "\\b";
						Pattern pattern = Pattern.compile(x);
						Matcher matcher = pattern.matcher(lowTxt);
						if (!matcher.find()) {
							containsAllPhrases = false;
						}
						if (containsAllPhrases) {
							for (String word : words) {
								String y = "\\b" + word + "\\b";
								Pattern pattern2 = Pattern.compile(y);
								Matcher matcher2 = pattern2.matcher(lowTxt);
								if (!matcher2.find()) {
									containsAllPhrases = false;
								}
							}
						}
					}
					if (containsAllPhrases) {
						results.add(ref);
					}
				}

			}
		}
		return new ReferenceList(results);
	}
}