package bibleReader.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Concordance is a class which implements a concordance for a Bible. In other
 * words, it allows the easy lookup of all references which contain a given
 * word.
 * 
 * @author Chuck Cusack, March 2013 (Provided the interface)
 * @author ?, March 2013 (Provided the implementation details)
 */
public class Concordance {
	private final TreeMap<String, TreeSet<Reference>> concordance;

	/**
	 * Construct a concordance for the given Bible.
	 */
	public Concordance(Bible bible) {
		concordance = new TreeMap<String, TreeSet<Reference>>();
		VerseList verses = bible.getAllVerses();
		for (Verse verse : verses) {
			ArrayList<String> split = extractWords(verse.getText());

			for (int i = 0; i < split.size(); i++) {
				String word = split.get(i);
				if (concordance.containsKey(word)) {
					concordance.get(word).add(verse.getReference());
				} else {
					TreeSet<Reference> references = new TreeSet<Reference>();
					concordance.put(word, references);
					concordance.get(word).add(verse.getReference());
				}

			}
		}

	}

	/**
	 * Return the list of references to verses that contain the word 'word'
	 * (ignoring case) in the version of the Bible that this concordance was created
	 * with.
	 * 
	 * @param word
	 *            a single word (no spaces, etc.)
	 * @return the list of References of verses from this version that contain the
	 *         word, or an empty list if no verses contain the word.
	 */
	public ReferenceList getReferencesContaining(String word) {
		// TODO: Implement me.
		// This one should only be a few lines if you implement this class correctly.
		TreeSet<Reference> result = concordance.get(word.toLowerCase());
		if (result == null) {
			return new ReferenceList();
		}
		ReferenceList trueResult = new ReferenceList(result);
		return trueResult;
	}

	/**
	 * Given an array of Strings, where each element of the array is expected to be a single word (with no spaces, etc.,
	 * but ignoring case), return a ReferenceList containing all of the verses that contain <i>all of the words</i>.
	 * 
	 * @param words A list of words.
	 * @return An ReferenceList containing references to all of the verses that contain all of the given words, or an
	 *         empty list if
	 */
	public ReferenceList getReferencesContainingAll(ArrayList<String> words) {
		// TODO: Implement me.
		// This one is a little more complicated, but is similar in many ways to methods you have already implemented.
		if(words.isEmpty()) {
			return new ReferenceList();
		}
		TreeSet<Reference> result = new TreeSet<Reference>();
		String firstWord = words.get(0);
		if(concordance.get(firstWord.toLowerCase()) == null) {
			return new ReferenceList();
		} else {
			result.addAll(concordance.get(firstWord.toLowerCase()));	
		}
		for(int i =1;i<words.size();i++) {
			TreeSet<Reference> temp = concordance.get(words.get(i).toLowerCase());
			
			if(temp!=null) {
				result.retainAll(temp);
			} else {
				return new ReferenceList();
			}
			
		}
		ReferenceList trueResult = new ReferenceList(result);
		return trueResult;
	}

	public static ArrayList<String> extractWords(String text) {
		text = text.toLowerCase();
		text = text.replaceAll("(<sup>[,\\w]*?</sup>|'s|'s|&#\\w*;)", " ");
		text = text.replaceAll(",", "");
		String[] words = text.split("\\W+");
		ArrayList<String> toRet = new ArrayList<String>(Arrays.asList(words));
		toRet.remove("");
		return toRet;
	}
}
