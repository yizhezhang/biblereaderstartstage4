package bibleReader.model;

import java.util.ArrayList;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface). Modified February 9, 2015.
 * @author Yizhe Zhang (provided the implementation)
 * @author Jason Gombas & Yizhe Zhang (Stage 7 and 9)
 */
public class ArrayListBible implements Bible {

	// The Fields
	private String version;
	private String title;
	private ArrayList<Verse> theVerses;
	private int find;

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param version
	 *            the version of the Bible (e.g. ESV, KJV, ASV, NIV).
	 * @param verses
	 *            All of the verses of this version of the Bible.
	 */
	public ArrayListBible(VerseList verses) {
		this.theVerses = new ArrayList<Verse>(verses);
		version = verses.getVersion();
		title = verses.getDescription();
		find = -1;
	}

	@Override
	public int getNumberOfVerses() {
		return theVerses.size();
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public boolean isValid(Reference ref) {
		for (Verse i : theVerses) {
			if (ref.equals(i.getReference())) {
				find = theVerses.indexOf(i);
				return true;
			}
		}
		return false;
	}

	@Override
	public String getVerseText(Reference r) {
		if (isValid(r) && find >= 0) {
			String x = theVerses.get(find).getText();
			find = -1;
			return x;
		}
		return null;
	}

	@Override
	public Verse getVerse(Reference r) {
		if (isValid(r) && find >= 0) {
			Verse x = theVerses.get(find);
			find = -1;
			return x;
		}
		return null;
	}

	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		Reference ref = new Reference(book, chapter, verse);
		return getVerse(ref);
	}

	@Override
	public VerseList getAllVerses() {
		return new VerseList(version, title, theVerses);
	}

	@Override
	public VerseList getVersesContaining(String phrase) {
		ArrayList<Verse> result = new ArrayList<Verse>();
		String newPhrase = phrase.toLowerCase();
		if (phrase != "") {
			for (Verse i : theVerses) {
				if ((i.getText().toLowerCase()).contains(newPhrase)) {
					result.add(i);
				}
			}
		}
		VerseList finalResult = new VerseList(version, title, result);
		return finalResult;
	}

	@Override
	public ReferenceList getReferencesContaining(String phrase) {
		ReferenceList result = new ReferenceList();
		String newPhrase = phrase.toLowerCase();
		if (!phrase.equals("")) {
			for (Verse i : theVerses) {
				if ((i.getText().toLowerCase()).contains(newPhrase)) {
					result.add(i.getReference());
				}
			}
		}
		return result;
	}

	@Override
	public VerseList getVerses(ReferenceList references) {
		ArrayList<Verse> result = new ArrayList<Verse>();
		for (Reference i : references) {
			result.add(getVerse(i));
		}
		VerseList finalResult = new VerseList(version, title, result);
		return finalResult;
	}

	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		for (int i = theVerses.size() - 1; i >= 0; i--) {
			Verse testVerse = theVerses.get(i);
			if (testVerse.getReference().getBookOfBible().equals(book)
					&& testVerse.getReference().getChapter() == chapter) {
				return testVerse.getReference().getVerse();
			}
		}
		return -1;
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		for (int i = theVerses.size() - 1; i >= 0; i--) {
			if (theVerses.get(i).getReference().getBookOfBible() == book) {
				return theVerses.get(i).getReference().getChapter();
			}
		}
		return -1;
	}

	@Override
	public ReferenceList getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		ReferenceList resultReferences = new ReferenceList();
		int firstIndex = -1;
		int max = theVerses.size();
		int i = 0;
		while (i < max) {
			if (firstVerse.equals(theVerses.get(i).getReference())) {
				firstIndex = i;
				i = max;
			}
			i++;
		}
		int j = firstIndex;
		if (j >= 0) {
			while (j < max) {
				resultReferences.add(theVerses.get(j).getReference());
				if (lastVerse.equals(theVerses.get(j).getReference()) && firstIndex >= 0) {
					j = max;
				}
				j++;
			}
		}
		if (j == max) {
			return new ReferenceList();
		}
		return resultReferences;
	}

	@Override
	public ReferenceList getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		ReferenceList resultReferences = new ReferenceList();
		if (firstVerse.compareTo(lastVerse) > 0 || !isValid(firstVerse)) {
			return resultReferences;
		}
		int i = find;
		int firstIndex = find;
		while (theVerses.get(i).getReference().compareTo(lastVerse) < 0) {
			i++;
		}
		int lastIndex = i;
		for (int j = firstIndex; j < lastIndex || j >= theVerses.size(); j++) {
			resultReferences.add(theVerses.get(j).getReference());
		}
		find = -1;
		return resultReferences;
	}

	@Override
	public ReferenceList getReferencesForBook(BookOfBible book) {
		Reference firstRef = new Reference(book, 1, 1);
		int lastChapterNumber = getLastChapterNumber(book);
		Reference lastRef = new Reference(book, lastChapterNumber, getLastVerseNumber(book, lastChapterNumber));
		return getReferencesInclusive(firstRef, lastRef);
	}

	@Override
	public ReferenceList getReferencesForChapter(BookOfBible book, int chapter) {
		Reference firstRef = new Reference(book, chapter, 1);
		Reference lastRef = new Reference(book, chapter, getLastVerseNumber(book, chapter));
		return getReferencesInclusive(firstRef, lastRef);
	}

	@Override
	public ReferenceList getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		Reference firstRef = new Reference(book, chapter1, 1);
		Reference lastRef = new Reference(book, chapter2, getLastVerseNumber(book, chapter2));
		return getReferencesInclusive(firstRef, lastRef);
	}

	@Override
	public ReferenceList getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		Reference firstRef = new Reference(book, chapter, verse1);
		Reference lastRef = new Reference(book, chapter, verse2);
		return getReferencesInclusive(firstRef, lastRef);
	}

	@Override
	public ReferenceList getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		Reference firstRef = new Reference(book, chapter1, verse1);
		Reference lastRef = new Reference(book, chapter2, verse2);
		return getReferencesInclusive(firstRef, lastRef);
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Verse> resultVerses = new ArrayList<Verse>();
		int firstIndex = -1;
		int lastIndex = -1;
		int max = theVerses.size();
		int i = 0;
		while (i < max) {
			if (firstVerse.equals(theVerses.get(i).getReference())) {
				firstIndex = i;
				i = max;
			}
			i++;
		}
		int j = firstIndex;
		if (j >= 0) {
			while (j < max) {
				if (lastVerse.equals(theVerses.get(j).getReference())) {
					lastIndex = j;
					j = max;
				}
				j++;
			}
		}
		if (firstIndex >= 0 && lastIndex >= 0) {
			for (int k = firstIndex; k <= lastIndex; k++) {
				resultVerses.add(theVerses.get(k));
			}
		}
		return new VerseList(version, getDescription(firstVerse, lastVerse), resultVerses);
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Verse> resultVerses = new ArrayList<Verse>();
		if (firstVerse.compareTo(lastVerse) > 0 || !isValid(firstVerse)) {
			return new VerseList(version, getDescription(firstVerse, lastVerse), resultVerses);
		}
		int i = find;
		int firstIndex = find;
		while (theVerses.get(i).getReference().compareTo(lastVerse) < 0) {
			i++;
		}
		int lastIndex = i;
		for (int j = firstIndex; j < lastIndex || j >= theVerses.size(); j++) {
			resultVerses.add(theVerses.get(j));
		}
		find = -1;
		return new VerseList(version, getDescription(firstVerse, lastVerse), resultVerses);
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		if (book == null) {
			return new VerseList(version, "invalid", new ArrayList<Verse>());
		}
		int lastChapter = getLastChapterNumber(book);
		Reference firstVerse = new Reference(book, 1, 1);
		Reference lastVerse = new Reference(book, lastChapter, getLastVerseNumber(book, lastChapter));
		return getVersesInclusive(firstVerse, lastVerse);
	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		Reference firstVerse = new Reference(book, chapter, 1);
		Reference lastVerse = new Reference(book, chapter, getLastVerseNumber(book, chapter));
		return getVersesInclusive(firstVerse, lastVerse);
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		Reference firstVerse = new Reference(book, chapter1, 1);
		Reference lastVerse = new Reference(book, chapter2, getLastVerseNumber(book, chapter2));
		return getVersesInclusive(firstVerse, lastVerse);
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		Reference firstVerse = new Reference(book, chapter, verse1);
		Reference lastVerse = new Reference(book, chapter, verse2);
		if (!isValid(firstVerse) || !isValid(lastVerse)) {
			return new VerseList(version, getDescription(firstVerse, lastVerse), new ArrayList<Verse>());
		}
		return getVersesInclusive(firstVerse, lastVerse);
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		Reference firstVerse = new Reference(book, chapter1, verse1);
		Reference lastVerse = new Reference(book, chapter2, verse2);
		return getVersesInclusive(firstVerse, lastVerse);
	}

	/**
	 * This is a helper method to get an appropriate description of a search given
	 * the two references.
	 * 
	 * @param ref1
	 * @param ref2
	 * @return
	 */
	private String getDescription(Reference ref1, Reference ref2) {
		BookOfBible book = ref1.getBookOfBible();
		int chapter1 = ref1.getChapter();
		int chapter2 = ref2.getChapter();
		int verse1 = ref1.getVerse();
		int verse2 = ref2.getVerse();
		int lastChapter = getLastChapterNumber(book);
		int lastVerse = getLastVerseNumber(book, chapter1);
		if (chapter1 == chapter2 && verse1 == verse2) {
			return book.toString() + chapter1 + ":" + verse1;
		}
		if (chapter1 == chapter2 && verse1 == 1 && verse2 == lastVerse) {
			return book.toString() + chapter1;
		}
		if (chapter1 == chapter2) {
			return book.toString() + chapter1 + ":" + verse1 + "-" + verse2;
		}
		if (chapter1 == 1 && chapter1 == 1 && chapter2 == lastChapter) {
			return book.toString();
		}
		if (chapter1 != chapter2) {
			return book.toString() + chapter1 + ":" + verse1 + "-" + chapter2 + ":" + verse2;
		}
		return "OOPS";
	}
}
