package bibleReader.model;

import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface)
 * @author ? (provided the implementation)
 */
public class TreeMapBible implements Bible {

	// The Fields
	private String version;
	private String title;
	private TreeMap<Reference, String> theVerses;

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param version
	 *            the version of the Bible (e.g. ESV, KJV, ASV, NIV).
	 * @param verses
	 *            All of the verses of this version of the Bible.
	 */
	public TreeMapBible(VerseList verses) {
		version = verses.getVersion();
		title = verses.getDescription();
		theVerses = new TreeMap<Reference, String>();
		for (Verse i : verses) {
			theVerses.put(i.getReference(), i.getText());
		}
	}

	@Override
	public int getNumberOfVerses() {
		return theVerses.size();
	}

	@Override
	public VerseList getAllVerses() {
		VerseList result = new VerseList(version, title);
		for (Reference ref : theVerses.keySet()) {
			result.add(new Verse(ref, theVerses.get(ref)));
		}
		return result;
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public boolean isValid(Reference ref) {
		if (ref != null) {
			if (ref.getBookOfBible() != null) {
				return theVerses.containsKey(ref);
			}
		}
		return false;
	}

	@Override
	public String getVerseText(Reference r) {
		return theVerses.get(r);
	}

	@Override
	public Verse getVerse(Reference r) {
		if (isValid(r)) {
			return new Verse(r, theVerses.get(r));
		} else {
			return null;
		}
	}

	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		return getVerse(new Reference(book, chapter, verse));
	}

	@Override
	public VerseList getVersesContaining(String phrase) {
		VerseList result = new VerseList(version, title);
		String newPhrase = phrase.toLowerCase();
		if (!newPhrase.equals("")) {
			for (Map.Entry<Reference, String> i : theVerses.entrySet()) {
				if (i.getValue().toLowerCase().contains(newPhrase)) {
					result.add(new Verse(i.getKey(), i.getValue()));
				}
			}
		}
		return result;
	}

	@Override
	public ReferenceList getReferencesContaining(String phrase) {
		ReferenceList result = new ReferenceList();
		String newPhrase = phrase.toLowerCase();
		if (!newPhrase.equals("")) {
			for (Map.Entry<Reference, String> i : theVerses.entrySet()) {
				if (i.getValue().toLowerCase().contains(newPhrase)) {
					result.add(i.getKey());
				}
			}
		}
		return result;
	}

	@Override
	public VerseList getVerses(ReferenceList references) {
		VerseList result = new VerseList(version, title);
		for (Reference ref : references) {
			result.add(getVerse(ref));
		}
		return result;
	}

	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		for (int i = 100; i > 0; i--) {
			if (isValid(new Reference(book, chapter, i))) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		for (int i = 200; i > 0; i--) {
			if (isValid(new Reference(book, i, 1))) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public ReferenceList getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		ReferenceList result = new ReferenceList();
		if (firstVerse.compareTo(lastVerse) > 0 || !isValid(firstVerse) || !isValid(lastVerse)) {
			return result;
		}
		SortedMap<Reference, String> s = theVerses.subMap(firstVerse, true, lastVerse, true);
		Set<Map.Entry<Reference, String>> mySet = s.entrySet();
		for (Map.Entry<Reference, String> element : mySet) {
			result.add(element.getKey());
		}
		return result;
	}

	@Override
	public ReferenceList getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		ReferenceList result = new ReferenceList();
		if (firstVerse.compareTo(lastVerse) > 0 || !isValid(firstVerse)) {
			return result;
		}
		SortedMap<Reference, String> s = theVerses.subMap(firstVerse, true, lastVerse, false);
		Set<Map.Entry<Reference, String>> mySet = s.entrySet();
		for (Map.Entry<Reference, String> element : mySet) {
			result.add(element.getKey());
		}
		return result;
	}

	@Override
	public ReferenceList getReferencesForBook(BookOfBible book) {
		Reference ref = new Reference(book, 1, 1);
		if (isValid(ref)) {
			return getReferencesExclusive(new Reference(book, 1, 1), new Reference(BookOfBible.nextBook(book), 1, 1));
		}
		return new ReferenceList();
	}

	@Override
	public ReferenceList getReferencesForChapter(BookOfBible book, int chapter) {
		return getReferencesExclusive(new Reference(book, chapter, 1), new Reference(book, chapter + 1, 1));
	}

	@Override
	public ReferenceList getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		return getReferencesExclusive(new Reference(book, chapter1, 1), new Reference(book, chapter2 + 1, 1));
	}

	@Override
	public ReferenceList getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		return getReferencesInclusive(new Reference(book, chapter, verse1), new Reference(book, chapter, verse2));
	}

	@Override
	public ReferenceList getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		return getReferencesInclusive(new Reference(book, chapter1, verse1), new Reference(book, chapter2, verse2));
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		VerseList someVerses = new VerseList(getVersion(), firstVerse + "-" + lastVerse);
		if (firstVerse.compareTo(lastVerse) > 0) {
			return someVerses;
		}
		SortedMap<Reference, String> s = theVerses.subMap(firstVerse, true, lastVerse, true);
		Set<Map.Entry<Reference, String>> mySet = s.entrySet();
		for (Map.Entry<Reference, String> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue());
			someVerses.add(aVerse);
		}
		return someVerses;
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		// Implementation of this method provided by Chuck Cusack.
		// This is provided so you have an example to help you get started
		// with the other methods.

		// We will store the resulting verses here. We copy the version from
		// this Bible and set the description to be the passage that was searched for.
		VerseList someVerses = new VerseList(getVersion(), firstVerse + "-" + lastVerse);

		// Make sure the references are in the correct order. If not, return an empty
		// list.
		if (firstVerse.compareTo(lastVerse) > 0) {
			return someVerses;
		}
		// Return the portion of the TreeMap that contains the verses between
		// the first and the last, not including the last.
		SortedMap<Reference, String> s = theVerses.subMap(firstVerse, lastVerse);

		// Get the entries from the map so we can iterate through them.
		Set<Map.Entry<Reference, String>> mySet = s.entrySet();

		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, String> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue());
			someVerses.add(aVerse);
		}
		return someVerses;
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		Reference ref = new Reference(book, 1, 1);
		if (isValid(ref)) {
			return getVersesExclusive(new Reference(book, 1, 1), new Reference(BookOfBible.nextBook(book), 1, 1));
		}
		return new VerseList(getVersion(), "");

	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		return getVersesExclusive(new Reference(book, chapter, 1), new Reference(book, chapter + 1, 1));
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		return getVersesExclusive(new Reference(book, chapter1, 1), new Reference(book, chapter2 + 1, 1));
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		return getVersesInclusive(new Reference(book, chapter, verse1), new Reference(book, chapter, verse2));
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		return getVersesInclusive(new Reference(book, chapter1, verse1), new Reference(book, chapter2, verse2));
	}

}
