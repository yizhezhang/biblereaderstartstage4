package bibleReader;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.WindowConstants;

import bibleReader.model.ArrayListBible;
import bibleReader.model.Bible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.VerseList;

/**
 * The main class for the Bible Reader Application.
 * 
 * @author cusack
 * @author Jonathan Chaffer & Jason Gombas (2018)
 * @author Jason Gombas & Yizhe Zhang (Stage 7, 9 and 10)
 */
public class BibleReaderApp extends JFrame {

	public static final int width = 800;
	public static final int height = 600;

	public static void main(String[] args) {
		new BibleReaderApp();
	}

	private BibleReaderModel model;
	private ResultView resultView;
	public JTextField inputField;
	public JButton searchButton;
	public JButton passageButton;

	private JFileChooser fc = null;

	private ActionEvent previousAction = null;
	private String previousString = null;

	/**
	 * Set-up the bible application and create the GUI.
	 */
	public BibleReaderApp() {
		// Load three default bibles
		model = new BibleReaderModel();
		File kjvFile = new File("kjv.atv");
		VerseList versesKJV = BibleIO.readBible(kjvFile);
		Bible kjv = new ArrayListBible(versesKJV);
		model.addBible(kjv);

		File esvFile = new File("esv.atv");
		VerseList versesESV = BibleIO.readBible(esvFile);
		Bible esv = new ArrayListBible(versesESV);
		model.addBible(esv);

		File asvFile = new File("asv.xmv");
		VerseList versesASV = BibleIO.readBible(asvFile);
		Bible asv = new ArrayListBible(versesASV);
		model.addBible(asv);

		setupLookAndFeel();

		setTitle("Bible Reader");

		resultView = new ResultView(model);
		inputField = new JTextField("", 20);
		inputField.setName("InputTextField");
		searchButton = new JButton("Search");
		searchButton.setName("SearchButton");
		passageButton = new JButton("Passage");
		passageButton.setName("PassageButton");

		// Add action listeners to inputField, searchButton, and passageButton
		inputField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				resultView.updateSearch(inputField.getText());
				previousAction = e;
				previousString = inputField.getText();
			}
		});

		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				resultView.updateSearch(inputField.getText());
				previousAction = e;
				previousString = inputField.getText();
			}
		});

		passageButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				resultView.updatePassage(inputField.getText());
				previousAction = e;
				previousString = inputField.getText();
			}
		});

		setupGUI();
		pack();
		setSize(width, height);

		// So the application exits when you click the "x".
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setVisible(true);
	}

	/**
	 * Set up the main GUI.
	 */
	private void setupGUI() {

		Container cont = getContentPane();
		cont.setLayout(new BorderLayout());

		// Setup the JMenuBar
		JMenuBar mainMenuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenu helpMenu = new JMenu("Help");
		JMenuItem openMenuItem = new JMenuItem("Open");
		openMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				importBible();
			}
		});
		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		JMenuItem aboutItem = new JMenuItem("About");
		aboutItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null,
						"This is a bible search application created by: \n" + "Jason Gombas and Yizhe Zhang");
			}
		});
		fileMenu.add(openMenuItem);
		fileMenu.add(exitItem);
		helpMenu.add(aboutItem);
		mainMenuBar.add(fileMenu);
		mainMenuBar.add(helpMenu);
		this.setJMenuBar(mainMenuBar);

		JPanel inputAndButtons = new JPanel();
		inputAndButtons.setLayout(new FlowLayout());
		cont.add(inputAndButtons, BorderLayout.NORTH);

		inputAndButtons.add(inputField);
		inputAndButtons.add(searchButton);
		inputAndButtons.add(passageButton);

		cont.add(resultView, BorderLayout.CENTER);
	}

	/**
	 * Imports a bible file from operating system, searches bible with the previous
	 * search and displays results
	 */
	private void importBible() {
		if (fc == null) {
			fc = new JFileChooser();
		}
		int returnVal = fc.showOpenDialog(this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			if (!file.exists()) {
				showFileNotFoundDialog();
			} else {
				model.addBible(new ArrayListBible(BibleIO.readBible(file)));
			}
		}
		if (previousString != null && previousAction != null) {
			if (previousAction.getSource().equals(inputField) || previousAction.getSource().equals(searchButton)) {
				resultView.updateSearch(previousString);
			} else if (previousAction.getSource().equals(passageButton)) {
				resultView.updatePassage(previousString);
			}
		}
	}

	private void showFileNotFoundDialog() {
		JOptionPane.showMessageDialog(this, "That file does not exist!", "File not found", JOptionPane.ERROR_MESSAGE);
	}

	// Method that enhances the look of the application
	private void setupLookAndFeel() {
		UIManager.put("control", new Color(241, 241, 212));
		UIManager.put("nimbusLisghtBackground", new Color(241, 241, 212));
		UIManager.put("nimbusFocus", new Color(241, 241, 212));
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {

		}
	}
}
